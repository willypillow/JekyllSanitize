JekyllSanitize
===
A simple Jekyll filter that uses the [sanitize](https://github.com/rgrove/sanitize) gem to sanitize HTML and CSS.

Usage
---
* `gem install sanitize` and add sanitize to your Gemfile.
* Copy sanitize.rb to the `_plugin` directory.
* `{{ variable_with_unsafe_html_as_output | sanitize }}`
