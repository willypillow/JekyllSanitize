require 'sanitize'

module Jekyll
  module SanitizeFilter
    def sanitize(input)
      Sanitize.fragment(input, Sanitize::Config::RELAXED)
    end
  end
end

Liquid::Template.register_filter(Jekyll::SanitizeFilter)

